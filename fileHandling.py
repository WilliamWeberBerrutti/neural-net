# -*- coding: utf-8 -*-

"""
  Este arquivo implementa a função de abertura dos arquivos de entrada para o trabalho
"""

from status import *
from random import *

"""
  Lê os arquivos de entrada, vindos do terminal.
  
  Entradas: filesList = lista de arquivos de dados, contém os arquivos de entrada da rede
  
  Saídas: resultFile = lista de lista de strings, são os arquivos lidos
          ERROR = booleano, indica problema na leitura de algum dos arquivos
"""
def openFiles(filesList):
    
    resultFiles = []
    filesAmount = len(filesList)
    
    for i in range(filesAmount):
        resultFiles.append([])
        resultFiles[i] = [line.rstrip('\n').split(';') for line in open(filesList[i])]
    
    if (len(resultFiles) == filesAmount):
        return resultFiles
    else:
        return ERROR

"""
  Gera os pesos aleatórios da Rede Neural
  
  Entradas: network_cfg = arquivo de dados, contém a configuração inicial da rede
  
  Saídas: strWeightsFile = lista de lista de reais, contém os pesos inicializados num intervalo pré-determinado
          ERROR = booleano, indica problema no processamento
"""
def generateWeights(netFile):

    strWeightsFile = ""
    
    for i in range(1, len(netFile)-1):
        for j in range(int(netFile[i+1])):
            for k in range(int(netFile[i])+1):
                strWeightsFile += str(round(uniform(-1.0,1.0),5))
                
                if (k < (int(netFile[i]))):
                    strWeightsFile += ","
                    
            if (j < int(netFile[i+1])-1):
                strWeightsFile += ";"
                
        if (i < len(netFile)-2):
            strWeightsFile += "\n"
    
    if (strWeightsFile):
        return strWeightsFile
    else:
        return ERROR
