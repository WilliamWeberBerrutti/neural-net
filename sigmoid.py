# -*- coding: utf-8 -*-

"""
  Este arquivo implementa a função que calcula a sigmoid
"""

from math import *

"""
  Calcula a sigmoide
  
  Entradas: z = número real, é a soma dos termos da função de ativação de todos os neurônios da camada anterior
  
  Saídas: número real
"""
def sigmoid(z):
    return (1 / (1 + exp(-z) ) )
