# -*- coding: utf-8 -*-

"""
  Este arquivo deixa o dataset no padrão exigido, transformando a saída
  em colunas binárias, e separando entrada e saída por ";"

  python3 preProcessDataset.py <input_file> <output_file>

  input_file: arquivo que contém o dataset que irá ser alterado
  output_file: arquivo de saída com o dataset padronizado
"""

from sys import *
from utilities import *

def main():

    if (len(argv) != 3):
        print("Usage: python3 preProcessDataset.py <input_file> <output_file>")
        return
    

    # Leitura do arquivo de entrada
    input_file = []
    input_file = [line.rstrip('\n').split(',') for line in open(argv[1])]
    input_file.pop(0)
    
    # Lista com os possiveis valores de saída
    output_values = []

    # Preenche a lista "output_values" com todos os possíveis valores de saída
    for i in range(len(input_file)):
        if (output_values.count(input_file[i][-1]) == 0):
            output_values.append(input_file[i][-1])
    
    # Lista auxiliar que guarda o cabeçalho e as saídas depois da conversão binária
    output_list = []
    
    # Cria as colunas com valores alvos e preenche de forma binaria
    for i in range(len(input_file)):
        output_list.append([])
        
        # Preenche as colunas com valores binários
        value = input_file[i][-1]
        input_file[i].pop(-1)
        for j in range(len(output_values)):
            if (value == output_values[j]):
                output_list[i].append("1")
            else:
                output_list[i].append("0")


    # Chamada da função para normalizar o dataset
    input_file = normalizer(input_file)
    
    # Lista para fazer as strings das linhas para o arquivo de saida
    output_string = []

    # Cria arquivo de saida
    for i in range(len(input_file)):
        # string auxiliar
        s = ",".join(input_file[i])
        s += ";"
        s += ",".join(output_list[i])

        output_string.append(s)


    # Escreve as strings no arquivo
    output_file = open(argv[2],"w")
    output_file.write("\n".join(output_string))
    output_file.close()

    '''
    DEBUG

    print("Input")
    print(*input_file,sep="\n")

    print("Output Values")
    print(*output_string,sep="\n")
    '''


main()
