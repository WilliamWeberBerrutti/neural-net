# -*- coding: utf-8 -*-

"""
  Este arquivo tem funções utilitárias necessárias ao trabalho
"""

from headerImport import *

"""
  Normaliza os valores de uma coluna
  
  Entradas: dataset = lista de lista (matriz) de elementos numéricos do dataset
  
  Saídas: resultDataset = lista de lista (matriz) de elementos numéricos normalizados do dataset.
                          É uma lista diferente da lista de entrada!
          ERROR = booleano, que indica erro no processo de normalização
"""
def normalizer(dataset):
    resultDataset = deepcopy(dataset)
    
    numRows = len(dataset)-1
    numCols = len(dataset[0])
    
    # DEBUG
    #print(numRows)
    #print(numCols)
    
    for col in range(numCols):
        
        maxElem = MIN_TEMP_VALUE
        minElem = MAX_TEMP_VALUE
        
        for row in range(numRows):
            
            if (maxElem < float(resultDataset[row][col])):
                maxElem = float(resultDataset[row][col])
                
            if (minElem > float(resultDataset[row][col])):
                minElem = float(resultDataset[row][col])
            
            # DEBUG
            #print("Iteração", row)
            #print(maxElem)
            #print(minElem)
            
        for row in range(numRows):
            # para não haver divisão por zero
            if (maxElem != minElem):
                resultDataset[row][col] = str((float(resultDataset[row][col]) - minElem) / (maxElem - minElem))

    if (resultDataset):
        return resultDataset
    else:
        return ERROR


"""
  Reduz a dimensionalidade de uma lista de listas
  
  Entradas: listToFlatten = lista de listas, para reduzir a dimensionalidade
  
  Saídas: lista de listas, com as dimensões reduzidas
"""
def flattenList(listToFlatten):
    return (list(chain.from_iterable(listToFlatten)))


"""
  Faz o pré-processamento dos arquivos de entrada, para facilitar seu uso no programa
  
  Entradas: netFile = lista de listas, é a configuração das camadas e neurônios da rede. A 1ª linha contém o fator de regularização da mesma
            weightsFile = lista de listas, são os pesos iniciais da rede (das arestas)
            dataFile = lista de listas, é o dataset a ser avaliado
            (DEPOIS) filesList = lista de arquivos de dados, contém os arquivos de entrada da rede
  
  Saídas: regularization_factor = número real, é o fator de regularização da rede
          netFile = lista de listas, é a configuração das camadas e neurônios da rede
          weightsFile = lista de listas, são os pesos iniciais da rede (das arestas)
          dataFile = lista de listas, é o dataset a ser avaliado
          (DEPOIS) resultFile = lista de lista de strings, são os arquivos pré-processados
"""
def preProcess(netFile, weightsFile, dataFile):#(filesList)
    
    ''' TENTANDO REFATORAR '''
    '''
    filesAmount = len(filesList)
    
    for i in range(len(filesList)):
        
        #fileLength = len(filesList[i])
        
        for j in range(len(filesList[i])):
            #resultFiles.append([])
            #resultFiles[i] = [line.rstrip('\n').split(';') for line in open(filesList[i])]
            #resultFiles[i] = [line.split(',') for line in open(filesList[i])]
            filesList[i][j][0] = filesList[i][j][0].split(",")
       
    ''' 
    # Separando o arquivo de pesos pelas virgulas e transformando em float
    
    for i in range(len(weightsFile)):
        for j in range(len(weightsFile[i])):
            weightsFile[i][j] = weightsFile[i][j].split(",")
            
            # Transforma cada número de weightsFile (após o split) em float
            for k in range(len(weightsFile[i][j])):
                weightsFile[i][j][k] = float(weightsFile[i][j][k])
    
    # fazendo o mesmo para dataFile
    for i in range(len(dataFile)):
        for j in range(len(dataFile[i])):
            dataFile[i][j] = dataFile[i][j].split(",")
            
            # Transforma cada número de dataFile (após o split) em float
            for k in range(len(dataFile[i][j])):
                dataFile[i][j][k] = float(dataFile[i][j][k])
    
    # agora para o netFile
    for i in range(len(netFile)):
        # Transforma cada número de netFile (após o split) em float
        for j in range(len(netFile[i])):
            netFile[i][j] = float(netFile[i][j])
    
    # Reduz a dimensionalidade ("achata") da netFile, para facilitar processamento e implementações
    netFile = flattenList(netFile)
    
    # Guarda o valor de regularização e dps o retira da lista de neurônios
    regularization_factor = float(netFile[0])
    
    return regularization_factor, netFile, weightsFile, dataFile
