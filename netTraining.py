# -*- coding: utf-8 -*-

"""
  Este arquivo faz o Backpropagation em loop
"""

from headerImport import *

#iteration = 1

"""
  Faz o treinamento da rede

  Entradas: neural_net = classe, representa a rede neural completa
            regularization_factor = número real, é o fator de regularização da rede
            netFile = lista de listas de string, é o arquivo de config da rede
            dataFile = lista de listas de string, é o arquivo de dados
            acceptableError = número real, é a taxa de erro aceitável para a execução do treinamento da rede neural
            alpha = número real, é a taxa de aprendizado da rede
            
  Saídas: neural_net = classe, é a rede neural treinada
"""
def netTraining(neural_net, regularization_factor, netFile, weightsFile, dataFile, test_fold, acceptableError, alpha):
    
    #global iteration
    
    old_J = 10000
    J_diff = 100
    
    # Escreve as strings no arquivo
    J_file = open("J_File.txt", "a") 
    
    if (not J_file):
        print("Erro ao abrir arquivo!")
        return ERROR
    
    iteration = 1
    
    # Critério de parada será quando o J variar menos de 1% de uma iteração para outra
    '''while (J_diff > acceptableError and iteration < 500):'''
    while (J_diff > acceptableError):
        
        # lista com as saidas da rede para cada linha da entrada
        r1 = neural_net.processEntry(dataFile, False)
        
        results = neural_net.classifyEntry(test_fold, False, False)
        
        # vetor com as saidas, utilizado para o calculo do J
        outputs = []
        for i in range(len(dataFile)):
            output = dataFile[i][len(dataFile[i])-1]
            outputs.append(output)
        
        '''
        print("R1")
        print(r1)
        print("RESULTS")
        print(results)
        '''
        # Chamada da função para cálculo do Custo J
        new_J = J(results, outputs, weightsFile, regularization_factor, False)
        
        # BackPropagation
        neural_net.finalGradient(regularization_factor, dataFile)

        # Calcula diferença entre J atual e J anterior
        J_diff = abs((old_J - new_J)/100)
        print("Diferença de J entre iterações", J_diff)

        # Se J_diff for maior que 1%, atualiza old_J e os pesos
        if (J_diff>acceptableError):
            old_J = new_J
            # Cria lista subtraindo os gradientes dos pesos
                #TODO
                #new_weights = []
            #print("WEIGHTS FILE BEFIRE")
            #print(*weightsFile,sep="\n")
            neural_net.updateWeights(alpha)
            #print("WEIGHTS FILE AFTER")
            #print(*weightsFile,sep="\n")
            #input()

            iteration += 1
        
        # DEBUG    
        print("J:", new_J)
        
        if ((iteration % 25) == 0):
            J_file.write(str(iteration) + ";" + str(new_J) + "\n")
    
    J_file.close()
    
    ''' Precisa retornar? '''
    #return iteration


"""
  Função que limpa o TestFold
"""
def clearTestFoldOutputs(outputs_testfold):
    while(len(outputs_testfold)):
        outputs_testfold.pop()


"""
  Função Main
  
  Como executar:
  
  $ python3 netTraining.py <network_cfg> <dataset_file> <acceptable Error> <alpha>
  
  onde:
  
  network_cfg: é a configuração das camadas e neurônios da rede. A 1ª linha contém o fator de regularização da mesma
  dataset_file: é o dataset a ser avaliado
  acceptable Error: é a taxa de erro aceitável para a execução do treinamento da rede neural
  alpha: é a taxa de aprendizado da rede
"""
def main():
    
    seed(1)
    
    if (len(argv) != 5):
        print("Usage: python3 netTraining.py <network_cfg> <dataset_file> <acceptable Error> <alpha>")
        return
        
    network_cfg = argv[1]
    dataset = argv[2]
    acceptableError = float(argv[3])
    alpha = float(argv[4])
    n_folds = 10

    # Definindo Seed
    seed(1)
    
    
    # Lê arquivos de config da rede
    netFile, dataFile = openFiles([network_cfg, dataset])

    # Só pra testes
    #netFile, weightsFile, dataFile = openFiles([network_cfg, weights_cfg, dataset])

    # Arruma o netFile para ser usado na função generateWeights
    netFile = flattenList(netFile)
    strWeights = generateWeights(netFile)

    # Desarruma o netFile para ser usada na função de preProcess
    netFile = [[netFile[i]] for i in range(len(netFile))]
    
    weightsFile = []
    weightsFile = strWeights.split('\n')
    
    for i in range(len(weightsFile)):
        weightsFile[i] = weightsFile[i].split(';')
    
    # Faz o pré-processamento dos arquivos
    regularization_factor, netFile, weightsFile, dataFile = preProcess(netFile, weightsFile, dataFile)
    
    fold_list = []
    fold_list = kFoldStratified(dataFile, n_folds)

    # Quantidade de entradas testadas é o tamanho de um fold
    # vezes a quantidade de testes que sera feito
    # (para os cálculos dos errorMeasures)
    tam_testfold = len(fold_list[0]) * n_folds
    
    
    # inicializa a matriz de confusão
    classes_amount = len(dataFile[0][1])
    initConfusionMatrix(classes_amount)
    
    print("CLASSES AMOUNT:",classes_amount)
    
    # DEBUG
    #printResultMatrix()
    
    neural_net = []
    
    print("==== Fazendo KFold Estratificado ====")
    
    # treinando os KFolds
    for i in range(n_folds):
        
        print("Num Fold:", i)
        
        aux_fold_list = []
        test_fold = []
        training_folds = []

        # copia a lista de folds para uma lista auxiliar
        aux_fold_list = deepcopy(fold_list)

        # pega o fold de teste
        test_fold = aux_fold_list[i]
        
        # DEBUG
        #print(*test_fold,sep="\n")
        #print("\n")
        #

        #print (*aux_fold_list,sep="\n")

        # pega os folds de treinamento
        aux_fold_list.remove(test_fold)

        # transforma lista de listas em uma lista só, para facilitar implementação
        for j in aux_fold_list:
            training_folds += j
            
        # Inicializa rede neural
        neural_net = NeuralNet()
        
        
        # Popula a rede neural com as informações lidas nos arquivos de config
        neural_net.populateNeuralNet(netFile, weightsFile)
        
        netTraining(neural_net, regularization_factor, netFile, weightsFile, training_folds, test_fold, acceptableError, alpha)
        
        # lista com as saidas da rede para cada linha da entrada
        
        classification_results = neural_net.classifyEntry(test_fold, False, True)
        
        outputs_testfold = []
        
        for i in range(len(test_fold)):
            outputs_testfold.append(test_fold[i].pop())
        
        processConfusionMatrix(outputs_testfold, classification_results)
        
        printConfusionMatrix()
        
        ''' NEW '''
        neural_net.eraseNeuralNet()
        clearTestFoldOutputs(outputs_testfold)
        ''' '''
        
    compactConfusionMatrix(classes_amount)
    
    printResultMatrix()
    
    
    #print(neural_net,sep="\n")
    # Faz as chamadas das funçoes de métrica
    Accuracy = round(accuracy(tam_testfold, classes_amount),5)
    Error = round(error(tam_testfold, classes_amount),5)
    Recall = round(recall(classes_amount),5)
    Precision = round(precision(classes_amount),5)
    Fmeasure = round(FMeasure(precision(classes_amount), recall(classes_amount), 1),5)

    # Converte resultados para string
    results = [alpha,Accuracy,Error,Recall,Precision,Fmeasure]
    results_string = ';'.join(str(x) for x in results)

    # DEBUG: impressão das medidas de erro
    compactConfusionMatrix(classes_amount)
    print("\n\n ===========================================")
    print("Num Folds: ", n_folds)
    print("RESULT MATRIX:")
    printResultMatrix()
    print("CONFUSION MATRIX:")
    printConfusionMatrix()
    print("Accuracy: ")
    print(Accuracy)
    print("Error: ")
    print(Error)
    print("Recall: ")
    print(Recall)
    print("Precision: ")
    print(Precision)
    print("FMeasure: ")
    print(Fmeasure)
    print("===========================================")

    # Escreve as strings no arquivo
    
    '''
    output_file = open("results.txt","a")
    output_file.write("\nLinha de comando:" + "\n" + str(argv) + "\n")
    output_file.write(results_string + "\n")
    output_file.close()
    '''

    # Limpando Matriz de Confusão
    resetConfusionMatrix(classes_amount)

main()
