# -*- coding: utf-8 -*-

"""
  Este arquivo tem as funcionalidades dos cálculos do J
"""

from headerImport import *

"""
  Função para cálculo do Custo J
  
  Entradas: results = lista com as saídas da rede
            outuputs = lista com as saídas esperadas
            weightsFile = lista de pesos, utilizado no calculo do J com regularização
            regularization_factor = utilizado no calculo com regularização
            printDebug = booleano, indica se serão impressos textos adicionais para debug
            
  Saidas: J_total = Custo J total da entrada
          ERROR = código de erro, indicando que não foi possível gerar J_list
"""
def J(results, outputs, weightsFile, regularization_factor, printDebug):
    J_list = []
    
    # loop para cada theta
    for i in range(len(results)):
        J = 0
        
        # loop para os casos de multiplas saídas, realiza o somatório
        for j in range(len(outputs[i])):
            J += -float(outputs[i][j])*(log(results[i][j]))-(1-float(outputs[i][j]))*(log(1-float(results[i][j])))

        # coloca o J de cada theta na lista de saída
        J_list.append(J)
        
    #DEBUG
    if (printDebug):
        for i in range(len(J_list)):
            print("\tJ do exemplo", i+1, ":", round(J_list[i],3))
    
    # Preparando os pesos para fazer o somatório de todos os pesos.
    aux_weight = deepcopy(weightsFile)
    
    # DEBUG
    '''
    print("AUX WEIGHT")
    print(id(aux_weight))
    print("WEIGHTS FILE")
    print(id(weightsFile))
    input()
    '''
    
    # Retira o peso dos Bias (Peso dos Bias não são utilizados na regularização)
    for i in range(len(aux_weight)):
        for j in range(len(aux_weight[i])):
            aux_weight[i][j].pop(0)

    # Eleva todos os pesos ao quadrado
    for i in range(len(aux_weight)):
        for j in range(len(aux_weight[i])):
            for k in range(len(aux_weight[i][j])):
                aux_weight[i][j][k] = float(aux_weight[i][j][k])**2

    # Somatório Pesos
    sum_weights = 0
    for i in range(len(aux_weight)):
        for j in range(len(aux_weight[i])):
            sum_weights += sum(aux_weight[i][j])
    
    # Somatório dos Custos J
    soma_J = sum(J_list)
    
    #Cálculo do J total
    J_total = (soma_J/len(J_list)) + (regularization_factor/(2*len(J_list)))*(sum_weights)
    
    if (printDebug):
        print("\nJ total do dataset (com regularização):", round(J_total,5))
    
    #print("J_TOTAL:", J_total)
    
    if (J_total):
        return J_total
    else:
        return ERROR


"""
  Faz a verificação numérica do J
  
  Entradas: netFile = lista de listas de string, é o arquivo de config da rede
            dataFile = lista de listas de string, é o arquivo de dados
            weightsFile = lista de listas de string, é o arquivo de pesos sinápticos
            regularization_factor = número real, é o fator de regularização da rede
  
  Saídas: n/a
"""
def J_numeric_verification(netFile, dataFile, weightsFile, regularization_factor):
    gradient_list = []
    epsilon = 0.000001

    # Inicializa a rede neural
    neural_net = NeuralNet()
    # Popula a rede neural com as informações lidas nos arquivos de config
    neural_net.populateNeuralNet(netFile, weightsFile)

    outputs = []

    for i in range(len(dataFile)):
        output = dataFile[i][len(dataFile[i])-1]
        outputs.append(output)


    for i in range(len(weightsFile)):
        gradient_list.append([])
        for j in range(len(weightsFile[i])):
            gradient_list[i].append([])
            for k in range(len(weightsFile[i][j])):


                # Atualiza os pesos após a soma do épsilon
                weightsFile[i][j][k] = weightsFile[i][j][k]+epsilon
                neural_net.changeWeights(weightsFile)                

                
                # lista com as saidas da rede para cada linha da entrada
                results = neural_net.processEntry(dataFile, False)

                J1 = J(results, outputs, weightsFile, regularization_factor, False)

                # Popula a rede neural com as informações lidas nos arquivos de config
                weightsFile[i][j][k] = weightsFile[i][j][k]-2*epsilon
                neural_net.changeWeights(weightsFile)
                
                # lista com as saidas da rede para cada linha da entrada
                results = neural_net.processEntry(dataFile, False)

                J2 = J(results, outputs, weightsFile, regularization_factor, False)

                gradient = (J1-J2)/(2*epsilon)
                gradient_list[i][j].append(round(gradient,5))

    print("--------------------------------------------")
    print("Rodando verificacao numérica de gradientes (epsilon=0.0000010000)")
    for i in range(len(gradient_list)):
        print("Grandiente numerico de Theta", i+1, ":")
        print(*gradient_list[i],sep="\n")
        print("\n")
