# -*- coding: utf-8 -*-

"""
  Este arquivo tem constantes para facilitar debugging e verificação da execução correta de funções
"""

""" Estados do programa """

SUCCESS = True      # função executou corretamente
ERROR = False       # algum erro no programa
FOUND = True        # um elemento foi encontrado

MIN_TEMP_VALUE = -1
MAX_TEMP_VALUE = 99999999
