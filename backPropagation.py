# -*- coding: utf-8 -*-

"""
  Este arquivo tem as funcionalidades do BackPropagation
"""

from headerImport import *

"""
  Função para exibir as configurações como no arquivo exemplo do moodle
"""
def debugConfigs(regularization_factor, netFile, weightsFile, dataFile):
    print("Parâmetro de regularização lambda =", round(regularization_factor,5))
    print("\nInicializando rede com a seguinte estrutura de neurônios por camadas:", netFile)
    for i in range(len(weightsFile)):
        s = weightsFile[i]
        print("\nTheta", i+1, "inicial:")
        print(*s,sep="\n")

    print("\n\nConjunto de treinamento")
    for i in range(len(dataFile)):
        print("\tExemplo", i+1)
        print("\t\tx:", dataFile[i][0])
        print("\t\ty:", dataFile[i][1])
    print("\n-----------------------------------------------")


"""
  Função Main
  
  Como executar:
  
  $ python3 backPropagation.py <network_cfg> <weights_cfg> <dataset_file> <print debug info? (1: Yes, 0: No)>
  
  onde:
  
  network_cfg: é a configuração das camadas e neurônios da rede. A 1ª linha contém o fator de regularização da mesma
  weights_cfg: são os pesos iniciais da rede (das arestas)
  dataset_file: é o dataset a ser avaliado
  print debug info?: imprime textos extras no terminal para debugging
"""
def main():
    
    if (len(argv) != 5):
        print ("Usage: python3 backPropagation.py <network_cfg> <weights_cfg> <dataset_file> <print debug info? (1: Yes, 0: No)>")
        return
    
    network_cfg = argv[1] #"network2.txt"
    weights_cfg = argv[2] #"initial_weights2.txt"
    dataset = argv[3] #"entry2.csv"
    printDebug = float(argv[4]) #1
    
    # Le os arquivos de configuração que estão em disco através da função openFiles()
    resultFiles = openFiles([network_cfg, weights_cfg, dataset])
  
    # Faz o pré-processamento dos arquivos
    regularization_factor, netFile, weightsFile, dataFile = preProcess(resultFiles[0], resultFiles[1], resultFiles[2])
    

    # DEBUG: Impressão dos conteúdos dos arquivos
    if (printDebug):
        print("Regularization Factor:", regularization_factor)
        print("netFile:", netFile)
        print("weightsFile:", weightsFile)
        print("dataFile:", dataFile)
        
    
    # Printando configs no padrao dos exemplos
    if (printDebug):
        debugConfigs(regularization_factor, netFile, weightsFile, dataFile)
        print("Calculando erro/custo J da rede")
    
    # Inicializa a rede neural
    neural_net = NeuralNet()
    
    # Popula a rede neural com as informações lidas nos arquivos de config
    neural_net.populateNeuralNet(netFile, weightsFile)
    
    # lista com as saidas da rede para cada linha da entrada
    results = neural_net.processEntry(dataFile, printDebug)

    # vetor com as saidas, utilizado para o calculo do J
    outputs = []

    # calculando o erro de cada execução
    error = []
    for i in range(len(results)):
        output = dataFile[i][len(dataFile[i])-1]
        #DEBUG
        print("\n\tSaída predita para o exemplo", i+1, ":", [round(elem,5) for elem in results[i]])
        print("\tSaída esperada para o exemplo", i+1, ":", output)

        outputs.append(output)
      
        for j in range(len(results[i])):
            error.append(results[i][j]-float(output[j]))


    # Chamada da função para cálculo do Custo J
    J_total = J(results, outputs, weightsFile, regularization_factor, printDebug)


    # BackPropagation
    neural_net.finalGradient(regularization_factor, dataFile)
    
    
    #DEBUG
    print("\tDataset completo processado. Calculando gradientes regularizados")
    for i in range(1, len(neural_net.gradient)):
        print("Gradientes finais para Theta", i, " (com regularização):")
        for j in range(len(neural_net.gradient[i])):
            print([round(elem,5) for elem in neural_net.gradient[i][j]])
        print("\n")


    J_numeric_verification(netFile, dataFile, weightsFile, regularization_factor)
    
    
    # Função para ver a quantidade de camadas e neurônios em cada camada
    if (printDebug):
        neural_net.debugNet()
        

main()
