# -*- coding: utf-8 -*-

"""
  Este arquivo tem as definições das classes e suas funções
"""

from sigmoid import *
from status import *
from random import *

layers = []     # Lista que em que cada index irá armazenar uma lista de neuronios
deltas = []	    # Matriz com os deltas. Utilizado para o cálculo do Backpropagation
gradient = []   # Lista de matrizes com os gradientes de Theta. Os gradientes serão
                # acumulados, e ao final da execução de todo o dataset, será somado
                # o fator de regularização e dividido pelo número de entradas.

neuron = []     # lista onde a 1ª entrada identifica se é bias ou não, a 2ª a lista
                # de pesos sinápticos da camada anterior, e a 3ª, a ativação do neurônio

# por ex: [0, [1,2,1], 0.75] representa um neurônio, que não é bias (0), a lista de pesos
# sinápticos é [1,2,1] referentes à camada anterior, e (0.75) é a ativação deste neurônio

'''
isBias = isBias    # se é o neurônio de bias ou não
weight = weight    # lista de pesos da camada anterior
activation = activation    # ativação do neurônio
'''

"""
  Função que inicializa a matriz de gradiente com zeros.
  Utiliza a quantidade de neurônios na camada anterior para
  preencher o gradiente com a quantidade correta de zeros.

  Entradas: n/a

  Saídas: n/a
"""
def populateGradient(self):
    for i in range(1, len(gradient)):
        for j in range(len(gradient[i])):
            gradient[i][j].extend([0]*(len(gradient[i-1])+1))


"""
  Função que popula a rede neural do sistema
      
  Entradas: neurons_per_layer = lista, onde cada índice da lista representa uma camada, e o valor da posição deste índice é a quantidade de neurônios existente na camada
            all_weights = lista, é a lista de pesos lida do arquivo de entrada
                
  Saídas: n/a
"""
def populateNeuralNet(self, neurons_per_layer, weights):

        for i in range(len(neurons_per_layer)-1):
            neuron_list = [] # lista de neuronios da camada i
            
            deltas.append([])
            gradient.append([])

            # Começa no indice -1, que sera a iteração onde o Bias sera adicionado
            for j in range(-1, int(neurons_per_layer[i+1])):
                # atribuição inicial pro caso da Input Layer
                weight = []
                
                
                # Quando não for a Input Layer, pega na lista de pesos da camada
                # os pesos correspondentes ao neurônio
                if (i != 0 and j >= 0):
                    weight = weights[i-1][j]

                # Na iteração j = -1 (para criação do Bias), se for a ultima camada não cria o Bias.
                if (j < 0 and i >= len(neurons_per_layer)-2):
                    continue

                # Cria um neuronio novo, indicando se ele é Bias, sua camada e os pesos
                if (j >= 0 and i < len(neurons_per_layer)):
                    new_neuron = Neuron(False, weight, 0.0)#Neuron(False, i, [uniform(-1.0,1.0)], 0.0)#
                    deltas[i].append(0) # Cria matriz de erros, para ser usada no cálculo do Backpropagation
                    gradient[i].append([]) # Adiciona na lista de gradientes da camada, uma lista para cada neuronio
                else:
                    new_neuron = Neuron(True, weight, 1)
                #print("I:", i, " Pesos:", new_neuron.weight)

                # Adiciona o neuronio na lista de neuronios da camada
                neuron_list.append(new_neuron)

            # Adiciona a lista de neuronios da camada na lista.
            # Ao final de execução, "layers" sera um vetor com todos os neuronios
            # de todas as camadas
            layers.append(neuron_list)

        populateGradient()


"""
  Função que é chamada após a propagação de cada entrada na rede.
  Ela vai acumulando os gradientes de cada entrada na matriz de gradiente.
  Após a propagação de todas as entradas, a matriz gradiente terá o somatório
  dos gradientes, que será utilizado depois para o cálculo do gradiente final.

  Entradas: n/a

  Saídas: n/a
"""
def calculateGradient(self):
    for i in reversed(range(1, len(layers))):
        for j in range(len(gradient[i])):
            for k in range(len(gradient[i][j])):
                gradient[i][j][k] += deltas[i][j]*float(layers[i-1][k].activation)  # tem que ver esta linha!


"""
  Função que compara a saída obtida com a saída esperada e calcula os 
  deltas, colocando-os na matriz de deltas da rede, que serão utilizados
  posteriormente para o cálculo do gradiente dessa entrada.

  Entradas: waited_output: lista com a saída esperada da respectiva entrada
                           propagada na rede.

  Saídas: n/a
"""
def calculateDeltas(self, waited_output):

    # uma iteração pra cada neuronio da ultima camada
    for i in range(len(layers[-1])):
        deltas[-1][i] = layers[-1][i].activation - float(waited_output[i])

    # loop para percorrer as camadas, de trás pra frente.
    # o delta do output já foi calculado, e o da input layer não é preciso
    for i in reversed(range(1, len(layers)-1)):

    # loop para percorrer todos os neuronios dentro da camada, exceto o de bias
        for j in range(0, len(layers[i])-1):
            delta_sum = 0

            if (i == len(layers)-2):
                for k in range(len(layers[i+1])):
                    delta_sum += deltas[i+1][k]*float(layers[i+1][k].weight[j+1])
            else:
                for k in range(len(layers[i+1])-1):
                    delta_sum += deltas[i+1][k]*float(layers[i+1][k+1].weight[j+1])

            deltas[i][j] = delta_sum*(layers[i][j+1].activation)*(1-layers[i][j+1].activation)


    calculateGradient()


"""
  Função que cálcula o gradiente final. Sua fórmula é dada por
    grad_final = (Somatório do Gradiente + Parcela de regularização) / n

  Entradas: regularization_factor: Fator de regularização. É dado na configuração
                                   da rede neural.
            dataset: conjunto de dados de entrada. É utilizado para descobrir
                     o "n" (tamanho do conjunto), que será utilizado no 
                     cálculo do gradiente.

  Saídas: n/a
"""
def finalGradient(self, regularization_factor, dataset):
    regularization_term = 0
    n = len(dataset)
    # loop para as camadas! Com i variando das ultimas camadas para a primeira
    for i in reversed(range(len(gradient))):
        # A ultima camada não tem o Bias, por isso muda a lógica da ultima para as demais
        if (i == len(gradient)-1):
            for j in range(len(gradient[i])):
                for k in range(len(gradient[i][j])):
                    regularization_term = float(layers[i][j].weight[k])*float(regularization_factor)
                    if (k == 0):
                        regularization_term = 0
                    gradient[i][j][k] += regularization_term
                    gradient[i][j][k] = gradient[i][j][k]/n
        else:
            for j in range(len(gradient[i])):
                for k in range(len(gradient[i][j])):
                    regularization_term = float(layers[i][j+1].weight[k])*float(regularization_factor)
                    if (k == 0):
                        regularization_term = 0
                    gradient[i][j][k] += regularization_term
                    gradient[i][j][k] = gradient[i][j][k]/n


"""
  Função que faz a classificação na Rede Neural
  
  Entradas: dataset = lista de listas, é o conjunto de dados para as entradas
            printDebug = booleano, indica se deve imprimir informações extras de debug no terminal
  
  Saidas: result_list = lista com as saidas da rede
          ERROR = booleano, indica erro na criação de result_list
"""
def classifyEntry(self, dataset, printDebug):
    result_list = []

    # loop para percorrer cada linha do dataset de entrada
    for n_entry in range(len(dataset)):

        #DEBUG
        if (printDebug):
            print("\tProcessando exemplo de treinamento", n_entry+1)
            print("\tPropagando entrada", dataset[n_entry][0])

        # preenche a Input Layer com os valores do dataset
        # -1 no controle é para pular o Bias
        entry = dataset[n_entry][0]
        for i in range(len(layers[0])-1):
            layers[0][i+1].activation = entry[i]

        for i in range(1, len(layers)):
            list_sum = []

            for j in range(len(layers[i])):
                # Não calcula sigmoid para o Bias
                if(layers[i][j].isBias == True):
                    continue
                sum_entrys = 0
                # i = percorre todas as camadas
                # j = percorre todos os neuronios dentro da camada
                # k = todas as entradas de um neuronio. É feito um somatorio com os
                # pesos e as ativações dos neuronios das camadas anteriores
                for k in range(len(layers[i][j].weight)):
                    sum_entrys += (float(layers[i-1][k].activation)*float(layers[i][j].weight[k]))
                

                # coloca o retorno da função sigmoid na ativação do neuronio
                layers[i][j].activation = sigmoid(sum_entrys)

        # Ativação do ultimo neurônio é inserida na lista de resultados
        result = []
        for tam in range(len(layers[-1])):
            result.append(round(layers[len(layers)-1][tam].activation, 0))

        result_list.append(result)

        #DEBUG
        if (printDebug):
            print("\n\t\tf(x):", [round(elem,5) for elem in result])
            print("\n                     ", dataset[n_entry][1])
            for k in range(len(result)):
                if (result[k] > 0.5 and dataset[n_entry][1][k] > 0.5):
                    print ("ACERTOU!")
                elif (result[k] <= 0.5 and dataset[n_entry][1][k] <= 0.5):
                    print ("ACERTOU!")
                else:
                    print ("ERROU!")
    
    
    # processa empates na classificação, garantindo que tenha apenas um "1" em cada avaliação de cada rede neural
    for i in range(len(result_list)):
        while (result_list[i].count(1.0) > 1):
            result_list[i][1+result_list[i].index(1.0)] = 0.0
        while (result_list[i].count(1.0) == 0):
            result_list[i][0] = 1.0
        
    if (result_list):
        return (result_list)
    else:
        return ERROR

    
"""
  Função que faz a propagação na Rede Neural, utilizando função sigmoid para o calculo de ativação.
  
  Entradas: dataset = lista de listas, é o conjunto de dados para as entradas.
            printDebug = booleano, indica se serão impressas informações extras de debug
  
  Saidas: result_list = lista de listas com as saidas da rede
          ERROR = booleano, indica erro na criação de result_list
"""
def processEntry(self, dataset, printDebug):
    result_list = []

    # loop para percorrer cada linha do dataset de entrada
    for n_entry in range(len(dataset)):

        #DEBUG
        if (printDebug):
            print("\tProcessando exemplo de treinamento", n_entry+1)
            print("\tPropagando entrada", dataset[n_entry][0])

        # preenche a Input Layer com os valores do dataset
        # -1 no controle é para pular o Bias
        entry = dataset[n_entry][0]
        for i in range(len(layers[0])-1):
            layers[0][i+1].activation = entry[i]

        #DEBUG
        if (printDebug):
            s = [float(layers[0][j].activation) for j in range(len(layers[0]))]
            print("\t\ta1:", [round(elem,5) for elem in s])

        for i in range(1, len(layers)):
            list_sum = []


            for j in range(len(layers[i])):
                # Não calcula sigmoide para o Bias
                if(layers[i][j].isBias == True):
                    continue
                sum_entrys = 0
                # i = percorre todas as camadas
                # j = percorre todos os neuronios dentro da camada
                # k = todas as entradas de um neuronio. É feito um somatorio com os
                # pesos e as ativações dos neuronios das camadas anteriores
                for k in range(len(layers[i][j].weight)):
                    sum_entrys += (float(layers[i-1][k].activation)*float(layers[i][j].weight[k]))
                

                # coloca o retorno da função sigmoid na ativação do neuronio
                layers[i][j].activation = sigmoid(sum_entrys)
                
                # Guardando nesta lista só para debug
                list_sum.append(sum_entrys)

            #DEBUG
            if (printDebug):
                print("\n\t\tz", i+1, ":", [round(elem,5) for elem in list_sum])
            
            #DEBUG
            if (printDebug):
                s = [float(layers[i][j].activation) for j in range(len(layers[i]))]
                print("\t\ta", i+1, ":", [round(elem,5) for elem in s])

        # Ativação do ultimo neurônio é inserida na lista de resultados
        result = []
        for tam in range(len(layers[-1])):
            result.append(layers[len(layers)-1][tam].activation)

        result_list.append(result)

        #DEBUG
        if (printDebug):
            print("\n\t\tf(x):", [round(elem,5) for elem in result])

        # A cada entrada, calcula os deltas e os acumula na matriz de deltas
        calculateDeltas(dataset[n_entry][1])
        
    if (result_list):
        return (result_list)
    else:
        return ERROR


"""
  Função que muda os pesos dos neurônios da rede.
  
  Entradas: weights = lista de reais, é a lista com os novos pesos para a rede.

  Saidas: n/a
"""
def changeWeights(self, weights):

    # loop para percorrer as camadas
    for i in range(len(weights)):
        # loop para percorrer os neuronios dentro das camadas
        for j in range(len(weights[i])):

            if (i != len(weights[i])):
                layers[i+1][j+1].weight = weights[i][j]
            else:
                layers[i+1][j].weight = weights[i][j]


"""
  Função que atualiza os pesos dos neurônios da rede.
  Para a atualização é usada a formula

  Peso = Peso - alpha * gradiente
  
  Entradas: alpha = número real, é taxa de aprendizado.
  
  Saidas: n/a
"""
def updateWeights(self, alpha):

    # loop para percorrer as camadas
    for i in range(len(layers)):
        # loop para percorrer os neuronios dentro das camadas
        for j in range(len(layers[i])):
            # loóp para percorrer os pesos do neuronio
            for k in range(len(layers[i][j].weight)):
                if(i != len(layers)-1):
                    #print("Peso", layers[i][j].weight[k])
                    layers[i][j].weight[k] = layers[i][j].weight[k] - alpha * gradient[i][j-1][k]
                    #print("Novo Peso", layers[i][j].weight[k])
                else:
                    #print("Peso",layers[i][j].weight[k])
                    layers[i][j].weight[k] = layers[i][j].weight[k] - alpha * gradient[i][j][k]
                    #print("Novo Peso", layers[i][j].weight[k])


"""
  Função de debug para Rede Neural
"""
def debugNet(self):
    for i in range(len(layers)):
        print(" -------------------------------------")
        print("Layer:", i)
        for j in range(len(layers[i])):
            print(layers[i][j].weight, round(float(layers[i][j].activation),5))
